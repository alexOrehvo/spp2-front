import { Injectable } from '@angular/core';
import {TaskResource} from '../web/rest/task.resource';
import {Task} from '../model/task.model';
import {TaskRepository} from '../model/task.repository';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private searchParameter: string;
  private comparator: any;

  constructor(
    private taskResource: TaskResource,
    private taskRepository: TaskRepository
  ) { }

  public getTasks(): Array<Task> {
    return this.taskRepository.getByName(this.searchParameter).sort(this.comparator);
  }

  public getById(id: number): Task {
    return this.taskRepository.getById(id);
  }

  public save(task: Task) {
    this.taskRepository.save(task);
  }

  public update(task: Task) {
    this.taskRepository.update(task);
  }

  public delete(id: number) {
    this.taskRepository.delete(id);
  }

  public setSearchParameter(parameter: string): void {
    this.searchParameter = parameter;
  }

  public getCurrentDate(): Date {
    return new Date();
  }

  public upload(task: Task) {
    this.taskRepository.uploadTask(task);
  }

  public deleteFile(taskId: number, fileUrl: string) {
    this.taskRepository.deleteFile(taskId, fileUrl);
  }

  public setComparator(comparator: any) {
    this.comparator = comparator;
  }
}

