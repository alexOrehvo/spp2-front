import { Injectable } from '@angular/core';
import {Task} from '../model/task.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceService {

  private hiddenTaskInfo = true;
  private selectedTaskId: number;

  constructor() { }

  toggleTaskInfo() {
    this.hiddenTaskInfo = !this.hiddenTaskInfo;
  }

  selectTask(taskId: number) {
    this.selectedTaskId = taskId;
  }

  isHiddenTaskInfo(): boolean {
    return this.hiddenTaskInfo;
  }

  getSelectedTaskId(): number {
    return this.selectedTaskId;
  }
}
