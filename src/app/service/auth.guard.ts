import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import {AccountService} from './account.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private accountService: AccountService,
    private router: Router
  ) { }

  canActivate(): boolean {
    if (this.accountService.loggedIn()) {
      return true;
    } else {
      console.log('false');
      this.router.navigateByUrl('/account/login');
      return false;
    }
  }
}
