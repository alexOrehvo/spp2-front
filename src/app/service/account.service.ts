import { Injectable } from '@angular/core';
import {AccountResource} from '../web/rest/account.resource';
import {Credentials} from '../model/credentials.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(
    private accountResource: AccountResource
  ) { }

  public signup(credentials: Credentials): Observable<any> {
    return this.accountResource.signup(credentials);
  }

  public login(credentials: Credentials): Observable<any> {
    return this.accountResource.login(credentials);
  }

  public logout() {
    localStorage.removeItem('token');
  }

  public loggedIn(): boolean {
    return !!localStorage.getItem('token');
  }
}
