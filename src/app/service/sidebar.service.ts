import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  private forms = {
    add: false,
    search: false,
    filter: false
  };

  private displayedSource = new BehaviorSubject(this.forms);

  public displayedForms = this.displayedSource.asObservable();

  constructor() { }

  hideAll() {
    this.forms.add = false;
    this.forms.search = false;
    this.forms.filter = false;
  }

  public toggleForm(formName: string) {
    this.hideAll();
    this.forms[formName] = !this.forms[formName];
    this.displayedSource.next(this.forms);
  }


}
