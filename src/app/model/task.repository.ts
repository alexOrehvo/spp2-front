import {Task} from './task.model';
import {Observable} from 'rxjs';
import {Injectable, OnInit} from '@angular/core';
import {TaskResource} from '../web/rest/task.resource';

@Injectable({
  providedIn: 'root'
})
export class TaskRepository implements OnInit {

  private tasks: Array<Task> = [];

  constructor(
    private taskResource: TaskResource
  ) {
    this.taskResource.getAll().subscribe(data => {
      console.log(data);
      this.tasks = data;
    });
  }

  ngOnInit(): void {

  }

  public getByName(name: string): Array<Task> {
    if (name == null || name === '') {
      return this.tasks;
    } else {
      return this.tasks.filter((t) => t.name === name);
    }
  }

  public getById(id: number): Task {
    return this.tasks.find(t => t.id === id);
  }

  public save(task: Task) {
    if (task.id == null || task.id === 0) {
      this.taskResource.save(task).subscribe(t => {
        this.tasks.push(t.createdTask);
      });
    } else {
      this.update(task);
    }
  }

  public delete(id: number): void {
    this.taskResource.delete(id).subscribe(task => {
      this.tasks.splice(this.tasks.findIndex(t => t.id === id), 1);
    });
  }

  public update(task: Task): void {
    this.taskResource.update(task).subscribe(updatedTask => {
      this.tasks.splice(this.tasks.findIndex(t => t.id === task.id), 1, updatedTask);
    });
  }

  public uploadTask(task: Task): void {
    console.log('uploadTask: ' + task.imageUrls);
    this.tasks.splice(this.tasks.findIndex(t => t.id === task.id), 1, task);
  }

  public deleteFile(taskId: number, fileUrl: string) {
    this.taskResource.deleteFile(taskId, fileUrl);
  }
}
