export class Task {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public done?: boolean,
    public startDate?: Date,
    public deadline?: Date,
    public imageUrls?: Array<string>,
    public isImportant?: boolean
  ) { }
}
