import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Credentials} from '../../model/credentials.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountResource {

  constructor(
    private http: HttpClient
  ) {}

  public signup(credentials: Credentials): Observable<any> {
    return this.http.post('http://localhost:3000/account/signup', credentials);
  }

  public login(credentials: Credentials): Observable<any> {
    return this.http.post('http://localhost:3000/account/login', credentials);
  }
}
