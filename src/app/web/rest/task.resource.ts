import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Task} from '../../model/task.model';

@Injectable({
  providedIn: 'root'
})
export class TaskResource {

  constructor(
    private http: HttpClient
  ) { }

  public getAll(): Observable<Task[]> {
    return this.http.get<Task[]>('http://localhost:3000/todo-list');
  }

  public getOneById(id: number): Observable<Task> {
    return this.http.get<Task>(`http://localhost:3000/todo-list/${id}`);
  }

  public save(task: Task): Observable<any> {
    return this.http.post<any>('http://localhost:3000/todo-list', task);
  }

  public delete(id: number): Observable<Task> {
    return this.http.delete<Task>(`http://localhost:3000/todo-list/${id}`);
  }

  public update(task: Task): Observable<Task> {
    console.log(task);
    return this.http.put('http://localhost:3000/todo-list/', task);
  }

  public deleteFile(id: number, imageUrl: string) {
    console.log('delete');
    this.http.delete(`http://localhost:3000/todo-list/${id}&${imageUrl}`);
  }
}
