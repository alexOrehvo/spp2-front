import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from '../../../../model/task.model';
import {Action, TaskListActions} from '../../../../core/model/action.model';
import {TaskService} from '../../../../service/task.service';
import {WorkspaceService} from '../../../../service/workspace.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  @Input()
  public task: Task;

  constructor(
    private taskService: TaskService,
    private workspaceService: WorkspaceService
  ) { }

  ngOnInit() {
  }

  changeImportance() {
    this.task.isImportant = !this.task.isImportant;
    this.taskService.update(this.task);
  }

  delete() {
    if (!this.workspaceService.isHiddenTaskInfo()) {
      this.workspaceService.toggleTaskInfo();
    }
    this.taskService.delete(this.task.id);
  }

  onDblClick() {
    this.workspaceService.selectTask(this.task.id);
    this.workspaceService.toggleTaskInfo();
  }

  onClick() {
    this.workspaceService.selectTask(this.task.id);
  }
}
