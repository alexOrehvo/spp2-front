import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from '../../../../model/task.model';
import {TaskService} from '../../../../service/task.service';
import {Action, TaskListActions} from '../../../../core/model/action.model';
import {WorkspaceService} from '../../../../service/workspace.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {

  @Input()
  private hiddenTaskInfo: boolean;

  @Input()
  private actions: Array<Action>;

  public showCompleted: boolean;

  public onlyImportant: boolean;

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit() {
    this.showCompleted = false;
    this.onlyImportant = false;
  }
  get tasks(): Array<Task> {
    return this.taskService.getTasks().filter(t =>
      (this.showCompleted || !t.done) && (!this.onlyImportant || t.isImportant)
    );
  }

  onShowCompleted() {
    this.showCompleted = !this.showCompleted;
  }

  onShowAll() {
    this.onlyImportant = false;
    this.taskService.setSearchParameter('');
  }

  onShowImportant() {
    this.onlyImportant = !this.onlyImportant;
  }
}
