import { Component, OnInit } from '@angular/core';
import {Task} from '../../../../model/task.model';
import {Action, TaskListActions} from '../../../../core/model/action.model';
import {TaskService} from '../../../../service/task.service';
import {WorkspaceService} from '../../../../service/workspace.service';

@Component({
  selector: 'app-workspace-layout',
  templateUrl: './workspace-layout.component.html',
  styleUrls: ['./workspace-layout.component.scss']
})
export class WorkspaceLayoutComponent implements OnInit {

  constructor(
    private taskService: TaskService,
    private workspaceService: WorkspaceService
  ) { }

  ngOnInit() {
  }

  get selectedTask(): Task {
    return this.taskService.getById(this.workspaceService.getSelectedTaskId());
  }

  get hiddenTaskInfo(): boolean {
    return this.workspaceService.isHiddenTaskInfo();
  }
}
