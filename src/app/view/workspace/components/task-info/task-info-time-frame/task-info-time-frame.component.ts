import {Component, Input, OnInit} from '@angular/core';
import {Task} from '../../../../../model/task.model';
import {TaskService} from '../../../../../service/task.service';

@Component({
  selector: 'app-task-time-frame',
  templateUrl: './task-info-time-frame.component.html',
  styleUrls: ['./task-info-time-frame.component.scss']
})
export class TaskInfoTimeFrameComponent implements OnInit {

  @Input()
  public task: Task;

  constructor(
    private taskService: TaskService
  ) {
  }

  ngOnInit() {
  }


  get currentDate(): Date {
    return this.taskService.getCurrentDate();
  }

  get deadline(): Date {
    return this.task.deadline ? new Date(this.task.deadline) : new Date();
  }

  get startDate(): Date {
    return this.task.startDate ? new Date(this.task.startDate) : new Date();
  }

  onChangeDeadline(event) {
    this.task.deadline = new Date(event.target.value);
    this.taskService.update(this.task);
  }
}
