import {Component, Input, OnInit} from '@angular/core';
import {Task} from '../../../../model/task.model';
import {WorkspaceService} from '../../../../service/workspace.service';
import {TaskService} from '../../../../service/task.service';

@Component({
  selector: 'app-task-info',
  templateUrl: './task-info.component.html',
  styleUrls: ['./task-info.component.scss']
})
export class TaskInfoComponent implements OnInit {

  @Input()
  private task: Task;

  constructor(
    private workspaceService: WorkspaceService,
    private taskService: TaskService
  ) { }

  ngOnInit() { }

  get hidden(): boolean {
    return this.workspaceService.isHiddenTaskInfo();
  }
}
