import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from '../../../../../model/task.model';
import {TaskService} from '../../../../../service/task.service';

@Component({
  selector: 'app-task-info-files',
  templateUrl: './task-info-files.component.html',
  styleUrls: ['./task-info-files.component.scss']
})
export class TaskInfoFilesComponent implements OnInit {

  @Input()
  private task: Task;

  private selectedImage: string = null;

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit() {
  }

  onDelete(taskId: number, index: number) {
    this.task.imageUrls.splice(index, 1);
    this.taskService.update(this.task);
  }

  onImageClick(imageUrl: string) {
    this.selectedImage = imageUrl;
  }

  onCloseModal() {
    this.selectedImage = null;
  }
}
