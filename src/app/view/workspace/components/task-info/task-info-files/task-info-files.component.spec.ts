import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskInfoFilesComponent } from './task-info-files.component';

describe('TaskInfoFilesComponent', () => {
  let component: TaskInfoFilesComponent;
  let fixture: ComponentFixture<TaskInfoFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskInfoFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskInfoFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
