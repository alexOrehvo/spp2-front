import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskInfoDescriptionComponent } from './task-info-description.component';

describe('TaskInfoDescriptionComponent', () => {
  let component: TaskInfoDescriptionComponent;
  let fixture: ComponentFixture<TaskInfoDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskInfoDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskInfoDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
