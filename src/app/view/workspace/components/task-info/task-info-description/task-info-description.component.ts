import {Component, Input, OnInit} from '@angular/core';
import {Task} from '../../../../../model/task.model';
import {TaskService} from '../../../../../service/task.service';

@Component({
  selector: 'app-task-info-description',
  templateUrl: './task-info-description.component.html',
  styleUrls: ['./task-info-description.component.scss']
})
export class TaskInfoDescriptionComponent implements OnInit {

  @Input()
  public task: Task;

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit() {
  }

 onChange() {
    this.taskService.update(this.task);
 }
}
