import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskComponent } from './components/task/task.component';
import {CustomFormsModule} from '../common/custom-forms.module';
import {FormsModule} from '@angular/forms';
import { TaskInfoDescriptionComponent } from './components/task-info/task-info-description/task-info-description.component';
import { TaskInfoTimeFrameComponent } from './components/task-info/task-info-time-frame/task-info-time-frame.component';
import { TaskInfoComponent } from './components/task-info/task-info.component';
import { WorkspaceLayoutComponent } from './components/workspace-layout/workspace-layout.component';
import { TaskInfoFilesComponent } from './components/task-info/task-info-files/task-info-files.component';
import {UploadModule} from '../upload/upload.module';

@NgModule({
  declarations: [
    TaskListComponent,
    TaskComponent,
    TaskInfoDescriptionComponent,
    TaskInfoTimeFrameComponent,
    TaskInfoComponent,
    WorkspaceLayoutComponent,
    TaskInfoFilesComponent
  ],
  imports: [
    CommonModule,
    CustomFormsModule,
    FormsModule,
    UploadModule
  ],
  exports: [
    WorkspaceLayoutComponent
  ]
})
export class WorkspaceModule { }
