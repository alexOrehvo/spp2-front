import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import {MainRoutingModule} from '../../routing/main-routing.module';
import {WorkspaceModule} from '../workspace/workspace.module';
import {CustomFormsModule} from '../common/custom-forms.module';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AddComponentComponent } from './components/header/forms/add-component/add-component.component';
import { SearchComponentComponent } from './components/header/forms/search-component/search-component.component';
import {FormsModule} from '@angular/forms';
import { FilterComponentComponent } from './components/header/forms/filter-component/filter-component.component';
import { SortedFieldComponent } from './components/header/forms/filter-component/sorted-field/sorted-field.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MainLayoutComponent,
    SidebarComponent,
    AddComponentComponent,
    SearchComponentComponent,
    FilterComponentComponent,
    SortedFieldComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    WorkspaceModule,
    CustomFormsModule,
    FormsModule
  ]
})
export class MainModule { }
