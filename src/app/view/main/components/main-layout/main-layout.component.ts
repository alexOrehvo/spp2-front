import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  private hiddenSidebar: boolean;

  constructor() { }

  ngOnInit() {
    this.hiddenSidebar = false;
  }

  toggleSidebar() {
    this.hiddenSidebar = !this.hiddenSidebar;
  }
}
