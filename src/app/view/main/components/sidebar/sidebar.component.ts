import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SidebarService} from '../../../../service/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input()
  private hidden: boolean;

  constructor(
    private sidebarService: SidebarService
  ) { }

  ngOnInit() {
  }

  onClick(formName: string) {
    this.sidebarService.toggleForm(formName);
  }
}
