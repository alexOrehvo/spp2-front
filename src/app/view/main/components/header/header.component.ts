import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {EventListener} from '@angular/core/src/debug/debug_node';
import {SidebarService} from '../../../../service/sidebar.service';
import {AccountService} from '../../../../service/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  /*Rename*/
  private showAddForm = false;
  private showSearchForm = false;
  private showFilterForm = false;

  private hidden: boolean;
  @Output()
  private hiddenSidebar = new EventEmitter<boolean>();

  constructor(
    private sidebarService: SidebarService,
    private accountService: AccountService,
    private router: Router
  ) { }

  ngOnInit() {
    this.hidden = false;
    this.sidebarService.displayedForms.subscribe(forms => {
      this.showAddForm = forms.add;
      this.showSearchForm = forms.search;
      this.showFilterForm = forms.filter;
    });
  }

  toggleSidebar() {
    this.hidden = !this.hidden;
    this.hiddenSidebar.emit(this.hidden);
  }

  logout() {
    this.accountService.logout();
    this.router.navigateByUrl('/account');
  }
}
