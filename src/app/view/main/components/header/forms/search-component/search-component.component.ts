import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../../../../../service/task.service';

@Component({
  selector: 'app-search-component',
  templateUrl: './search-component.component.html',
  styleUrls: ['./search-component.component.scss']
})
export class SearchComponentComponent implements OnInit {

  private taskName: string;

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit() {
  }

  search() {
    this.taskService.setSearchParameter(this.taskName);
  }
}
