import { Component, OnInit } from '@angular/core';
import {TaskService} from '../../../../../../service/task.service';
import {Task} from '../../../../../../model/task.model';

@Component({
  selector: 'app-add-component',
  templateUrl: './add-component.component.html',
  styleUrls: ['./add-component.component.scss']
})
export class AddComponentComponent implements OnInit {

  private taskName: string;

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit() {
  }

  createTask() {
    const task = new Task();
    task.name = this.taskName;
    task.startDate = new Date();
    task.done = false;
    this.taskService.save(task);
    this.taskName = '';
  }
}
