import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortedFieldComponent } from './sorted-field.component';

describe('SortedFieldComponent', () => {
  let component: SortedFieldComponent;
  let fixture: ComponentFixture<SortedFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortedFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortedFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
