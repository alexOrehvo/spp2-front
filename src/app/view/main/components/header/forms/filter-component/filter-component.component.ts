import { Component, OnInit } from '@angular/core';
import {Task} from '../../../../../../model/task.model';

@Component({
  selector: 'app-filter-component',
  templateUrl: './filter-component.component.html',
  styleUrls: ['./filter-component.component.scss']
})
export class FilterComponentComponent implements OnInit {

  filterFields: FilterFields[] = [
    {
      title: 'Name',
      comparators: [
        (t1: Task, t2: Task) => {return t1.name.localeCompare(t2.name);},
        (t1: Task, t2: Task) => {return t2.name.localeCompare(t1.name);}
      ]
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}

interface FilterFields {
  title: string;
  comparators: any[];
}
