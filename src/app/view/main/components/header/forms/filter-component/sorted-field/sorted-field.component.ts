import {Component, Input, OnInit} from '@angular/core';
import {TaskService} from '../../../../../../../service/task.service';

@Component({
  selector: 'app-sorted-field',
  templateUrl: './sorted-field.component.html',
  styleUrls: ['./sorted-field.component.scss']
})
export class SortedFieldComponent implements OnInit {

  @Input()
  private title: string;

  @Input()
  private comparators = [];

  private ascending = true;

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit() {
  }

  public toggle() {
    if (this.ascending) {
      this.taskService.setComparator(this.comparators[0]);
    } else {
      this.taskService.setComparator(this.comparators[1]);
    }
    this.ascending = !this.ascending;
  }
}
