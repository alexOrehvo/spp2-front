import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../../service/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  private email: string;
  private password: string;
  private confirmedPassword: string;
  private errorMessage: string;

  constructor(
    private accountService: AccountService,
    private router: Router
  ) { }

  ngOnInit() {
    this.errorMessage = '';
  }

  signup() {
    if (this.password !== this.confirmedPassword) {
      this.errorMessage = 'Passwords are not equal';
    } else {
      this.errorMessage = '';
      this.accountService.signup({
        email: this.email,
        password: this.password
      }).subscribe(data => {
        this.accountService.login({email:this.email,
          password: this.password}).subscribe(
          res => {
            if (res.token) {
              localStorage.setItem('token', res.token);
              this.router.navigateByUrl('/main');
            } else {
              this.errorMessage = res.message;
            }
          },
          err => {
            console.log(err);
          });
      },
        error => this.errorMessage = error.message
      );
    }
  }

}
