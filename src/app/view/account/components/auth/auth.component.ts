import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../../service/account.service';
import {Credentials} from '../../../../model/credentials.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  private credentials: Credentials = new Credentials();
  private errorMessage: string;

  constructor(
    private accountService: AccountService,
    private router: Router
  ) { }

  ngOnInit() {
    this.credentials.email = '';
    this.credentials.password = '';
    this.errorMessage = '';
  }

  login() {
    this.accountService.login(this.credentials).subscribe(
      res => {
        if (res.token) {
          localStorage.setItem('token', res.token);
          this.router.navigateByUrl('/main');
        } else {
          this.errorMessage = res.message;
        }
      },
      err => {
        console.log(err);
      });
  }
}
