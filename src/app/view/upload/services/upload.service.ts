import {Injectable} from '@angular/core';
import {HttpClient, HttpEventType} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Task} from '../../../model/task.model';
import {TaskService} from '../../../service/task.service';
import {WorkspaceService} from '../../../service/workspace.service';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(
    private http: HttpClient,
    private taskService: TaskService
  ) { }

  public uploadFile(id: number, file: File) {
    const formData = new FormData();
    formData.append('image', file, file.name);
    this.http.post(`http://localhost:3000/todo-list/upload/${id}`, formData)
      .subscribe(res => {
       this.taskService.upload(<Task>res);
      });
  }
}
