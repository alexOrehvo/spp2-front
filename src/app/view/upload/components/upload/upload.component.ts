import {Component, Input, OnInit} from '@angular/core';
import {UploadService} from '../../services/upload.service';
import {TaskService} from '../../../../service/task.service';
import {WorkspaceService} from '../../../../service/workspace.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  @Input()
  private id: number;

  private selectedFile: File = null;

  constructor(
    private uploadService: UploadService,
    private taskSerive: TaskService,
    private workspaceService: WorkspaceService
  ) { }

  ngOnInit() {
  }

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
  }

  onUpload() {
    if (this.selectedFile != null) {
      this.uploadService.uploadFile(this.id, this.selectedFile);
      this.selectedFile = null;
    }
  }

  onReset() {
    this.selectedFile = null;
  }
}
