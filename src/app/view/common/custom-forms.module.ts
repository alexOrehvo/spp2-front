import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import {FormsModule} from '@angular/forms';
import { InputTextComponent } from './components/input-text/input-text.component';
import { PanelComponent } from './components/panel/panel.component';
import { ModalWindowComponent } from './components/modal-window/modal-window.component';

@NgModule({
  declarations: [
    CheckboxComponent,
    InputTextComponent,
    PanelComponent,
    ModalWindowComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    CheckboxComponent,
    InputTextComponent,
    PanelComponent,
    ModalWindowComponent
  ]
})
export class CustomFormsModule { }
