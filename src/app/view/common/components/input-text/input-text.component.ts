import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent implements OnInit {

  @Input()
  private leftButtonStyle: string;

  @Input()
  private rightButtonStyle: string;

  @Input()
  private placeholder: string;

  @Input()
  private type: string;

  /*@Output()
  private actions = new EventEmitter<>();*/

  private value: string;


  constructor() { }

  ngOnInit() {
    this.value = '';
  }

}
