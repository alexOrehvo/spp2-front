import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ControlValueAccessor} from '@angular/forms';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {

  @Input()
  public value: boolean;

  @Output()
  public valueChange = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  toggle(model: boolean) {
    this.value = model;
    this.sleep(200).then(() =>
      this.valueChange.emit(model));
  }

  sleep (time) {
    return new Promise((resolve) => setTimeout(resolve, time));
  }
}

interface Styles {
  checked: string;
  unchecked: string;
}
