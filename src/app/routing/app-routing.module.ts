import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from '../app.component';
import {AuthGuard} from '../service/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: 'main',
        loadChildren: '../view/main/main.module#MainModule',
        canActivate: [ AuthGuard ]
      },
      {
        path: 'account',
        loadChildren: '../view/account/account.module#AccountModule',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
