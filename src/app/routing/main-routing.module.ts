import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MainLayoutComponent} from '../view/main/components/main-layout/main-layout.component';
import {AuthGuard} from '../service/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
