export class Action {
  constructor(
    public title: string,
    public value?: any
  ) { }
}

export enum TaskListActions {
  TOGGLE_TASK_INFO = 'TOGGLE_TASK_INFO'
}
